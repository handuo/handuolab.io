---
title: Probabilistic Models
subtitle: (1) HMM
date: "2018-06-04"
tags: ["Machine Learning", "Notes"]
---
Hidden Markov Model 隐马尔可夫模型
The task is to estimate and infer the interested unknown variables(e.g. labels and classes) according to observed evidence (e.g. training samples). In probabilistic model, we can simplify the learning task into calculating the probabilistic distribution of the target variables. The key to **Inference** from known variables to the distribution of unknown variables is how to estimate the conditional distribution from observed variables.

Assume the target variable set is $Y$, the observable variable set is $O$, and other variable set $R$. `Generative model` considers joint distribution $P(Y, R, O) while `discriminative model` considers probabilistic distribution $P(Y, R | R)$. The aim is to acquire the conditional probabilistic distribution $P(Y | O)$.

