---
title: Meaning of different metrics in Object Detection (1)
subtitle: Calculation of mAP for PASCAL VOC Challenge
date: 2018-07-06
tags: ["Machine Learning", "Notes"]
---

### 1. Definition

### 1.1 Interesect of Union (IoU)

average instersect of union of objects and detections for a certain threshold (like 0.5)

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="-thumb" link="/img/iou_illus.jpeg" caption="IoU Illustration" >}}
{{< /gallery >}}
{{< load-photoswipe >}}


### 1.2 Average Precision (AP)

For VOC2007 challenge, AP was used to evaluate both classification and detection.

For a given task and class, the precision/recall curve is computed from a method’s ranked output. Recall is defined as the proportion of all positive examples ranked above a given rank. Precision is the proportion of all examples above that rank which are from the positive class. The AP summarises the shape of the precision/recall curve, and is defined as the mean precision at a set of eleven equally spaced recall levels [0,0.1,...,1]: 
`$AP = \frac{1}{11} \sum_{r\in{0,0.1,...,1}} P_{interp}(r)$`.

The precision at each recall level `r` is interpolated by taking the maximum precision measured for a method for which the corresponding recall exceeds r:` $P_{interp}(r) = max_{\widetilde{r}\geq{r}}p(r˜)$`, where `$p(\widetilde{r})$` is the measured precision at recall `$\widetilde{r}$`.