---
title: Pooling Layer in CNN (1)
subtitle: Prerequsites of pooling layer
date: 2018-05-02
---

Today I didn't have the mood to continue my work on map merging of different cameras. So I read the paper from DeepMind of `Learned Deformation Stability in Convolutional Neural Networks` recommended by [Wang Chen](https://wangchen.online/).

## 1. Convolution Operation

Convolution operation is typically denoted with an asterisk[^fn1]:
$$
s(t)=(x*w)(t)
$$
In Convolutional network terminology, the *x* is referred to as the **input**, and the *w* as the **kernel**. The output is sometimes referred to as the **feature map**.

---------------------------------------------------------

Convolution leverages three ideas that help improve the ML system: **sparse interactions**, **parameter sharing** and **equivariant representations**. Moreover, convolution provides a means for working with inputs of variable size.

#### - Sparse interactions

Also called sparse connectivity or sparse weights, makes the kernel smaller than the input. By detecting small, meaningful features such as edges with kernels (tens or hundreds of pixels), we don't need to process all the input pixels(millions), which means much fewer parameters.

<table>
    <tbody>
        <tr>
            <td>
                <img src="https://gitlab.com/handuo/msc_storage/uploads/4c0bd1f6e4a69d35c8390362fbbca78a/sparse_connectivity.png" alt="Sparse connectivity" width="80%">
            </td>
            <td>
                <img src="https://gitlab.com/handuo/msc_storage/uploads/bf6611fd0b7646dcc68358a173be3388/param_sharing.png" alt="Parameter sharing" width="100%">
            </td>
        </tr>
        <tr align="center">
            <td>Sparse connectivity</td>
            <td>Parameter sharing</td>
        </td>
    </tbody>
</table>

#### - Parameter sharing

Refers to using the same parameter for more than one function in a model, so a network has **tied weights**. This reduces the storage requirements of the model to *k* parameters.

#### - Equivariance to translation

But not to other transformations like scale or rotation change.

---------------------------------------------------------

## 2. Pooling 

In all cases, pooling helps to make the representation become approximately invariant to small translations of the input. Pooling over spatial regions produces invariance to translation, but if we pool over the outputs of separately parametrized convolutions, the features can learn which transformations to become invariant to.

Because pooling summarizes the responses over a whole neighborhood, it is
possible to use fewer pooling units than detector units, by reporting summary
statistics for pooling regions spaced k pixels apart rather than 1 pixel apart.







[^fn1]: Ian Goodfellow, Yoshua Bengio, and Aaron Courville, **Deep Learning**, 2016.