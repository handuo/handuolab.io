## Research Summary

I am currently a Ph.D student in Nanyang Technological University. My research interest is 3D Robot Vision and Visual SLAM. Currently I am involved in a stereo vison based project for unmanned ground vehicle.

The aim of the page is to summarize my ideas, which might include `Deep Learning`, `Visual SLAM`, `3D Mapping`, `Object Detection`, etc.

Head over to the [Github page](https://zhanghanduo.github.io) to see my detailed introduction.

