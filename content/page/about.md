---
title: Zhang Handuo
subtitle: Visual SLAM & 3D Robot Vision
comments: false
---

&emsp; I am currently a Ph.D student in Nanyang Technological University. My research interest is Robot Vision and Visual SLAM. Currently I am involved in a stereo vison based project for unmanned ground vehicle.

---------------------------------------------
---------------------------------------------
### Experience
---------------------------------------------
#### Organization
---------------------------------------------

- **Shenyang Institute of Automation Chinese Academy of Sciences (SIA)**

---------------------------------------------
**Assistant Researcher &bull; Aug, 2013 &mdash; Jul, 2015**

&emsp; I worked in the State Key Laboratory of Robotics in SIA. I was in charge of robot communication and control algorithms. During the two years, I took part in five projects and two of them are supported by nation-level programs.

---------------------------------------------
#### Education
---------------------------------------------

- **Nanyang Technological University**

**Ph.D Student on Robot vision and visual positioning. &bull; Sep, 2016 &mdash; Present**

&emsp; Now I am a 3nd year student trying to publish some papers.

---------------------------------------------
- **Northeastern University**

**Master on Pattern Recognition and Intelligent System. &bull;  Sep, 2011 &mdash; Jul, 2013.**

Top 1% GPA (3.51).

**Bachelor on Automation.&bull; Sep, 2007 &mdash; Jul, 2011**

Top 15% GPA (3.4).

---------------------------------------------
---------------------------------------------
### Publications
---------------------------------------------
#### Journals
---------------------------------------------

- Shiping Wang, Handuo Zhang, Han Wang. Object co-segmentation via weakly supervised data fusion, Computer Vision and Image Understanding (CVIU), 2016.

- Chengdong Wu, Jinchen Sun, Handuo Zhang. An Improved Polynomials Model Using RBF Network for Fish-eye Lens Correction, Journal of Information &Computational Science, 2012, 9(13), 3655-3663.

- Yunzhou Zhang, Shanbao Yang, Xiaolin Su, Enyi Shi, Handuo Zhang: Automatic reading of domestic electric meter: an intelligent device based on image processing and ZigBee/Ethernet communication. J. Real-Time Image Processing 12(1): 133-143 (2016)


#### Conferences
---------------------------------------------
- Handuo Zhang, Hasith Karunasekera, Han Wang. A Hybrid Feature Parametrization for Improving Stereo-SLAM Consistency. In IEEE International Conference on Control & Automation 2017. (Accepted)

- Hasith Karunasekera, Handuo Zhang, Tao Xi, Han Wang. Stereo Vision based Negative Obstacle Detection. In IEEE International Conference on Control & Automation 2017. (Accepted)

<!-- Chen Wang, Handuo Zhang, Tete Ji, Junsong Yuan, Lihua Xie. General Kernelized Correlation Filters. ICCV 2017. (Submitted) -->

- Chen Wang, Handuo Zhang, Le Zhang, Lihua Xie. Ultra-Wideband Aided Fast Localization and Mapping System. IROS 2017. (Accepted)

- Yunzhou Zhang, Liqiang Li, Handuo Zhang. Design of Wireless Multimedia Sensor Networks Nodes Based on DM642[C]. The 7th China Conference on Wireless Sensor Networks (CWSN2013).

- Guowei Zhang, Bin Li, Zhiqiang Li, Cong Wang, Handuo Zhang, Hong Shang, Weijian Hu, Tao Zhang: Development of robotic spreader for earthquake rescue. SSRR 2014: 1-5




<!-- just watch [my movie](https://en.wikipedia.org/wiki/The_Big_Lebowski) and it
will answer **all** your questions. -->
